﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace FantasyTeams.Generator.Storage
{
    public interface IConfigProvider
    {
        Task<(int minAge, int maxAge)> GetAgeGaps();
        Task<(int minPriceIncrease, int maxPriceIncrease)> GetPriceIncreaseGaps();
        Task<int> GetTeamStartBalance();
        Task<int> GetPlayerStartValue();
        Task<int> GetGoalkeepersCount();
        Task<int> GetDefendersCount();
        Task<int> GetMidfieldersCount();
        Task<int> GetAttackersCount();
        Task<bool> GetTeamAutoGenerationEnabled();

        Task SetAgeGaps((int minAge, int maxAge) value);
        Task SetPriceIncreaseGaps((int minPriceIncrease, int maxPriceIncrease) value);
        Task SetTeamStartBalance(int value);
        Task SetPlayerStartValue(int value);
        Task SetGoalkeepersCount(int value);
        Task SetDefendersCount(int value);
        Task SetMidfieldersCount(int value);
        Task SetAttackersCount(int value);
        Task SetTeamAutoGenerationEnabled(bool value);
    }
}