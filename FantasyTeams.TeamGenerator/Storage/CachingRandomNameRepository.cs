﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FantasyTeams.Generator.Models;
using Microsoft.Extensions.Caching.Memory;

namespace FantasyTeams.Generator.Storage
{
    public class CachingRandomNameRepository : IRandomNameRepository
    {
        private static string Key = "RandomNameCache";

        private readonly IRandomNameRepository _repository;
        private readonly IMemoryCache _memoryCache;

        public CachingRandomNameRepository(IRandomNameRepository repository, IMemoryCache memoryCache)
        {
            _repository = repository;
            _memoryCache = memoryCache;
        }

        public async Task<IEnumerable<RandomName>> GetRandomNames()
        {
            if (!_memoryCache.TryGetValue(Key, out Dictionary<int, RandomName> cache))
            {
                cache = (await _repository.GetRandomNames()).ToDictionary(x => x.Id);
                _memoryCache.Set(Key, cache);
            }

            return cache.Values;
        }

        public async Task<IEnumerable<RandomName>> GetRandomNames(NameType nameType)
        {
            return (await GetRandomNames()).Where(x => x.NameType == nameType);
        }

        public async Task<RandomName> GetRandomName(int id)
        {
            return (await GetRandomNames()).SingleOrDefault(x => x.Id == id);
        }

        public ValueTask AddName(RandomName name)
        {
            if (_memoryCache.TryGetValue(Key, out Dictionary<int, RandomName> cache))
            {
                cache[name.Id] = name;
                _memoryCache.Set(Key, cache);
            }

            return _repository.AddName(name);
        }

        public void DeleteRandomName(RandomName name)
        {
            if (_memoryCache.TryGetValue(Key, out Dictionary<int, RandomName> cache))
            {
                cache.Remove(name.Id);
                _memoryCache.Set(Key, cache);
            }

            _repository.DeleteRandomName(name);
        }

        public Task<bool> SaveChanges()
        {
            return _repository.SaveChanges();
        }
    }
}