﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FantasyTeams.Data.Storage;
using FantasyTeams.Generator.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace FantasyTeams.Generator.Storage
{
    public class RandomNameRepository : IRandomNameRepository
    {
        private readonly GeneratorDbContext _context;
        private readonly ILogger _logger;

        public RandomNameRepository(GeneratorDbContext context, ILogger<RandomNameRepository> logger)
        {
            _context = context;
            _logger = logger;
        }

        public async Task<IEnumerable<RandomName>> GetRandomNames()
        {
            return await _context.RandomName.ToListAsync();
        }

        public async Task<IEnumerable<RandomName>> GetRandomNames(NameType nameType)
        {
            return await _context.RandomName.Where(x => x.NameType == nameType).ToListAsync();
        }

        public Task<RandomName> GetRandomName(int id)
        {
            return _context.RandomName.FirstOrDefaultAsync(x => x.Id == id);
        }

        public async ValueTask AddName(RandomName name)
        {
            await _context.RandomName.AddAsync(name);
        }

        public void DeleteRandomName(RandomName name)
        {
            _context.RandomName.Remove(name);
        }

        public async Task<bool> SaveChanges()
        {
            try
            {
                return await _context.SaveChangesAsync() > 0;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Failed to save changes");
                throw;
            }
        }
    }
}
