﻿using System.Linq;
using System.Threading.Tasks;
using FantasyTeams.Generator.Models;
using Microsoft.EntityFrameworkCore;

namespace FantasyTeams.Generator.Storage
{
    public class ConfigProvider : IConfigProvider
    {
        private readonly GeneratorDbContext _context;

        public ConfigProvider(GeneratorDbContext context)
        {
            _context = context;
        }

        public async Task<(int minAge, int maxAge)> GetAgeGaps()
        {
            return (
                await GetConfig<int>(ConfigConstants.MinAgeKey),
                await GetConfig<int>(ConfigConstants.MaxAgeKey)
            );
        }

        public async Task<(int minPriceIncrease, int maxPriceIncrease)> GetPriceIncreaseGaps()
        {
            return (
                await GetConfig<int>(ConfigConstants.MinPriceIncreaseKey),
                await GetConfig<int>(ConfigConstants.MaxPriceIncreaseKey)
            );
        }

        public Task<int> GetTeamStartBalance()
        {
            return GetConfig<int>(ConfigConstants.TeamStartBalanceKey);
        }

        public Task<int> GetPlayerStartValue()
        {
            return GetConfig<int>(ConfigConstants.PlayerStartValueKey);
        }

        public Task<int> GetGoalkeepersCount()
        {
            return GetConfig<int>(ConfigConstants.GoalkeepersCountKey);
        }

        public Task<int> GetDefendersCount()
        {
            return GetConfig<int>(ConfigConstants.DefendersCountKey);
        }

        public Task<int> GetMidfieldersCount()
        {
            return GetConfig<int>(ConfigConstants.MidfieldersCountKey);
        }

        public Task<int> GetAttackersCount()
        {
            return GetConfig<int>(ConfigConstants.AttackersCountKey);
        }

        public async Task<bool> GetTeamAutoGenerationEnabled()
        {
            return await GetConfig<int>(ConfigConstants.AutoTeamGenerationKey) == 1;
        }

        public Task SetAgeGaps((int minAge, int maxAge) value)
        {
            return SetConfig(new Config(ConfigConstants.MinAgeKey, value.minAge),
                new Config(ConfigConstants.MaxAgeKey, value.maxAge));
        }

        public Task SetPriceIncreaseGaps((int minPriceIncrease, int maxPriceIncrease) value)
        {
            return SetConfig(new Config(ConfigConstants.MinPriceIncreaseKey, value.minPriceIncrease),
                new Config(ConfigConstants.MaxPriceIncreaseKey, value.maxPriceIncrease));
        }

        public Task SetTeamStartBalance(int value)
        {
            return SetConfig(new Config(ConfigConstants.TeamStartBalanceKey, value));
        }

        public Task SetPlayerStartValue(int value)
        {
            return SetConfig(new Config(ConfigConstants.PlayerStartValueKey, value));
        }

        public Task SetGoalkeepersCount(int value)
        {
            return SetConfig(new Config(ConfigConstants.GoalkeepersCountKey, value));
        }

        public Task SetDefendersCount(int value)
        {
            return SetConfig(new Config(ConfigConstants.DefendersCountKey, value));
        }

        public Task SetMidfieldersCount(int value)
        {
            return SetConfig(new Config(ConfigConstants.MidfieldersCountKey, value));
        }

        public Task SetAttackersCount(int value)
        {
            return SetConfig(new Config(ConfigConstants.AttackersCountKey, value));
        }

        public Task SetTeamAutoGenerationEnabled(bool value)
        {
            var intValue = value ? 1 : 0;
            return SetConfig(new Config(ConfigConstants.AutoTeamGenerationKey, intValue));
        }

        private async Task SetConfig(params Config[] configs)
        {
            foreach (var config in configs)
            {
                Config configToUpdate = await _context.Configs.SingleOrDefaultAsync(x => x.Key == config.Key);
                
                if (configToUpdate != null)
                {
                    configToUpdate.Value = config.Value;
                }
                else
                {
                    await _context.Configs.AddAsync(config);
                }
            }

            await _context.SaveChangesAsync();
        }

        private Task<T> GetConfig<T>(string key)
        {
            return _context.Configs.Where(x => x.Key == key).Select(x => x.Value).Cast<T>().FirstOrDefaultAsync();
        }
    }
}