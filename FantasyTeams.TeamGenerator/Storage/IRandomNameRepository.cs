﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FantasyTeams.Generator.Models;

namespace FantasyTeams.Generator.Storage
{
    public interface IRandomNameRepository
    {
        Task<IEnumerable<RandomName>> GetRandomNames();
        Task<IEnumerable<RandomName>> GetRandomNames(NameType nameType);
        Task<RandomName> GetRandomName(int id);
        ValueTask AddName(RandomName name);
        void DeleteRandomName(RandomName name);
        Task<bool> SaveChanges();
    }
}