﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FantasyTeams.Generator.Models;
using Microsoft.Extensions.Caching.Memory;

namespace FantasyTeams.Generator.Storage
{
    public class CachingConfigProviderDecorator : IConfigProvider
    {
        private readonly IConfigProvider _configProvider;
        private readonly IMemoryCache _memoryCache;

        public CachingConfigProviderDecorator(IConfigProvider configProvider, IMemoryCache memoryCache)
        {
            _configProvider = configProvider;
            _memoryCache = memoryCache;
        }

        public Task<(int minAge, int maxAge)> GetAgeGaps()
        {
            return GetConfig(ConfigConstants.AgeGapKey, _configProvider.GetAgeGaps);
        }

        public Task<(int minPriceIncrease, int maxPriceIncrease)> GetPriceIncreaseGaps()
        {
            return GetConfig(ConfigConstants.PriceIncreaseGapKey, _configProvider.GetPriceIncreaseGaps);
        }

        public Task<int> GetTeamStartBalance()
        {
            return GetConfig(ConfigConstants.TeamStartBalanceKey, _configProvider.GetTeamStartBalance);
        }

        public Task<int> GetPlayerStartValue()
        {
            return GetConfig(ConfigConstants.PlayerStartValueKey, _configProvider.GetPlayerStartValue);
        }

        public Task<int> GetGoalkeepersCount()
        {
            return GetConfig(ConfigConstants.GoalkeepersCountKey, _configProvider.GetGoalkeepersCount);
        }

        public Task<int> GetDefendersCount()
        {
            return GetConfig(ConfigConstants.DefendersCountKey, _configProvider.GetDefendersCount);
        }

        public Task<int> GetMidfieldersCount()
        {
            return GetConfig(ConfigConstants.MidfieldersCountKey, _configProvider.GetMidfieldersCount);
        }

        public Task<int> GetAttackersCount()
        {
            return GetConfig(ConfigConstants.AttackersCountKey, _configProvider.GetAttackersCount);
        }

        public Task<bool> GetTeamAutoGenerationEnabled()
        {
            return GetConfig(ConfigConstants.AutoTeamGenerationKey, _configProvider.GetTeamAutoGenerationEnabled);
        }

        public Task SetAgeGaps((int minAge, int maxAge) value)
        {
            return SetConfig(ConfigConstants.AgeGapKey, value, _configProvider.SetAgeGaps);
        }

        public Task SetPriceIncreaseGaps((int minPriceIncrease, int maxPriceIncrease) value)
        {
            return SetConfig(ConfigConstants.PriceIncreaseGapKey, value, _configProvider.SetPriceIncreaseGaps);
        }

        public Task SetTeamStartBalance(int value)
        {
            return SetConfig(ConfigConstants.TeamStartBalanceKey, value, _configProvider.SetTeamStartBalance);
        }

        public Task SetPlayerStartValue(int value)
        {
            return SetConfig(ConfigConstants.PlayerStartValueKey, value, _configProvider.SetPlayerStartValue);
        }

        public Task SetGoalkeepersCount(int value)
        {
            return SetConfig(ConfigConstants.GoalkeepersCountKey, value, _configProvider.SetGoalkeepersCount);
        }

        public Task SetDefendersCount(int value)
        {
            return SetConfig(ConfigConstants.DefendersCountKey, value, _configProvider.SetDefendersCount);
        }

        public Task SetMidfieldersCount(int value)
        {
            return SetConfig(ConfigConstants.MidfieldersCountKey, value, _configProvider.SetMidfieldersCount);
        }

        public Task SetAttackersCount(int value)
        {
            return SetConfig(ConfigConstants.AttackersCountKey, value, _configProvider.SetAttackersCount);
        }

        public Task SetTeamAutoGenerationEnabled(bool value)
        {
            return SetConfig(ConfigConstants.AutoTeamGenerationKey, value, _configProvider.SetTeamAutoGenerationEnabled);
        }

        private Task SetConfig<T>(string key, T value, Func<T, Task> setter)
        {
            _memoryCache.Set(key, value);
            return setter(value);
        }

        private async Task<T> GetConfig<T>(string configKey, Func<Task<T>> getter)
        {
            if (!_memoryCache.TryGetValue(ConfigConstants.AgeGapKey, out T cache))
            {
                cache = await getter();
                _memoryCache.Set(configKey, cache);
            }

            return cache;
        }
    }
}