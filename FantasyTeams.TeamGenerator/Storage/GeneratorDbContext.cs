﻿using System.Collections.Generic;
using System.Diagnostics;
using FantasyTeams.Generator.Models;
using Microsoft.EntityFrameworkCore;

namespace FantasyTeams.Generator.Storage
{
    public class GeneratorDbContext : DbContext
    {
        public DbSet<Config> Configs { get; set; }

        public DbSet<RandomName> RandomName { get; set; }

        public GeneratorDbContext(DbContextOptions<GeneratorDbContext> options)
            : base(options)
        {
        }

        [Conditional("DEBUG")]
        public static void TestSeed(GeneratorDbContext context)
        {
            var configs = new List<Config>
            {
                new Config(ConfigConstants.MinAgeKey, 18),
                new Config(ConfigConstants.MaxAgeKey, 40),
                new Config(ConfigConstants.MinPriceIncreaseKey, 10),
                new Config(ConfigConstants.MaxPriceIncreaseKey, 100),
                new Config(ConfigConstants.PlayerStartValueKey, 1000000),
                new Config(ConfigConstants.TeamStartBalanceKey, 5000000),
                new Config(ConfigConstants.GoalkeepersCountKey, 3),
                new Config(ConfigConstants.DefendersCountKey, 6),
                new Config(ConfigConstants.MidfieldersCountKey, 6),
                new Config(ConfigConstants.AttackersCountKey, 5),
                new Config(ConfigConstants.AutoTeamGenerationKey, 1)
            };
            context.Configs.AddRange(configs);

            var firstNames = new List<RandomName>
            {
                new RandomName("Alan", NameType.FirstName),
                new RandomName("Patrick", NameType.FirstName),
                new RandomName("George", NameType.FirstName),
                new RandomName("Robert", NameType.FirstName),
                new RandomName("Jeremy", NameType.FirstName),
                new RandomName("Mark", NameType.FirstName),
                new RandomName("Jim", NameType.FirstName),
                new RandomName("Adam", NameType.FirstName),
                new RandomName("Oliver", NameType.FirstName),
                new RandomName("William", NameType.FirstName),
                new RandomName("James", NameType.FirstName),
                new RandomName("Lucas", NameType.FirstName),
                new RandomName("Henry", NameType.FirstName),
                new RandomName("Kyle", NameType.FirstName),
                new RandomName("Ian", NameType.FirstName),
                new RandomName("Sean", NameType.FirstName),
                new RandomName("John", NameType.FirstName),
            };
            context.RandomName.AddRange(firstNames);

            var lastNames = new List<RandomName>
            {
                new RandomName("Smith", NameType.LastName),
                new RandomName("Green", NameType.LastName),
                new RandomName("Cooper", NameType.LastName),
                new RandomName("Morgan", NameType.LastName),
                new RandomName("Brown", NameType.LastName),
                new RandomName("Jones", NameType.LastName),
                new RandomName("Hill", NameType.LastName),
                new RandomName("Freeman", NameType.LastName),
                new RandomName("Johnson", NameType.LastName),
                new RandomName("Williams", NameType.LastName),
                new RandomName("Miller", NameType.LastName),
                new RandomName("Davis", NameType.LastName),
                new RandomName("Wilson", NameType.LastName),
                new RandomName("Jackson", NameType.LastName),
                new RandomName("Martin", NameType.LastName),
                new RandomName("White", NameType.LastName),
                new RandomName("Harris", NameType.LastName),
            };
            context.RandomName.AddRange(lastNames);

            var firstParts = new List<RandomName>
            {
                new RandomName("Quick", NameType.TeamFirstPart),
                new RandomName("Black", NameType.TeamFirstPart),
                new RandomName("Fast", NameType.TeamFirstPart),
                new RandomName("Red", NameType.TeamFirstPart),
                new RandomName("Big", NameType.TeamFirstPart),
                new RandomName("Mighty", NameType.TeamFirstPart),
                new RandomName("Great", NameType.TeamFirstPart),
                new RandomName("Magic", NameType.TeamFirstPart)
            };
            context.RandomName.AddRange(firstParts);

            var secondParts = new List<RandomName>
            {
                new RandomName("Pandas", NameType.TeamSecondPart),
                new RandomName("Cats", NameType.TeamSecondPart),
                new RandomName("Beavers", NameType.TeamSecondPart),
                new RandomName("Panthers", NameType.TeamSecondPart),
                new RandomName("Bees", NameType.TeamSecondPart),
                new RandomName("Monkeys", NameType.TeamSecondPart),
                new RandomName("Cows", NameType.TeamSecondPart),
                new RandomName("Hawks", NameType.TeamSecondPart)
            };
            context.RandomName.AddRange(secondParts);

            context.SaveChanges();
        }
    }
}
