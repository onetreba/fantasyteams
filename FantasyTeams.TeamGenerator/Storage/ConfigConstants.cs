﻿namespace FantasyTeams.Generator.Storage
{
    static class ConfigConstants
    {
        public const string AgeGapKey = "AgeGap";
        public const string PriceIncreaseGapKey = "PriceIncreaseGap";

        public const string MinPriceIncreaseKey = "PriceIncrease";
        public const string MaxPriceIncreaseKey = "PriceIncrease";
        public const string MinAgeKey = "MinAge";
        public const string MaxAgeKey = "MaxAge";
        public const string PlayerStartValueKey = "PlayerStartValue";
        public const string TeamStartBalanceKey = "TeamStartBalance";
        public const string GoalkeepersCountKey = "GoalkeepersCount";
        public const string DefendersCountKey = "DefendersCount";
        public const string MidfieldersCountKey = "MidfieldersCount";
        public const string AttackersCountKey = "AttackersCount";

        public const string AutoTeamGenerationKey = "AutoTeamGenerationEnabled";
    }
}