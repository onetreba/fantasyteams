﻿using System.Threading.Tasks;
using FantasyTeams.Data.Models;
using FantasyTeams.Generator.Models;

namespace FantasyTeams.Generator
{
    public interface IRandomValueProvider
    {
        Task<string> GetRandomValue(NameType type);
        Task<Country> GetRandomCountry();
        Task<uint> GetRandomAge();
        Task<int> GetRandomPriceIncreasePercent();
    }
}