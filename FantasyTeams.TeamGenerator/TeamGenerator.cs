﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FantasyTeams.Data.Models;
using FantasyTeams.Data.Storage;
using FantasyTeams.Generator.Models;
using FantasyTeams.Generator.Storage;

namespace FantasyTeams.Generator
{
    public class TeamGenerator : ITeamGenerator
    {
        private readonly IConfigProvider _configProvider;
        private readonly IRandomValueProvider _randomValueProvider;

        public TeamGenerator(IConfigProvider configProvider, IRandomValueProvider randomValueProvider)
        {
            _configProvider = configProvider;
            _randomValueProvider = randomValueProvider;
        }

        public async Task<Team> GenerateTeam()
        {
            return new Team(await GenerateTeamName())
            {
                CountryId = (await _randomValueProvider.GetRandomCountry()).Id,
                Balance = await _configProvider.GetTeamStartBalance(),
                Players = await GeneratePlayers()
            };
        }

        private async Task<string> GenerateTeamName()
        {
            var firstPart = await _randomValueProvider.GetRandomValue(NameType.TeamFirstPart);
            var secondPart = await _randomValueProvider.GetRandomValue(NameType.TeamSecondPart);
            return $"{firstPart} {secondPart}";
        }

        private async Task<List<Player>> GeneratePlayers()
        {
            var players = new List<Player>();

            players.AddRange(await GeneratePlayers(await _configProvider.GetGoalkeepersCount(), Position.Goalkeeper));
            players.AddRange(await GeneratePlayers(await _configProvider.GetDefendersCount(), Position.Defender));
            players.AddRange(await GeneratePlayers(await _configProvider.GetMidfieldersCount(), Position.Midfielder));
            players.AddRange(await GeneratePlayers(await _configProvider.GetAttackersCount(), Position.Attacker));

            return players;
        }

        private async Task<List<Player>> GeneratePlayers(int count, Position position)
        {
            var players = new List<Player>();

            for (int i = 0; i < count; i++)
            {
                players.Add(await GeneratePlayer(position));
            }

            return players;
        }

        private async Task<Player> GeneratePlayer(Position position)
        {
            return new Player
            {
                Position = position,
                MarketValue = await _configProvider.GetPlayerStartValue(),
                Age = await _randomValueProvider.GetRandomAge(),
                FirstName = await _randomValueProvider.GetRandomValue(NameType.FirstName),
                LastName = await _randomValueProvider.GetRandomValue(NameType.LastName),
                CountryId = (await _randomValueProvider.GetRandomCountry()).Id
            };
        }
    }
}
