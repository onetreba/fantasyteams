﻿using System;
using System.Linq;
using System.Threading.Tasks;
using FantasyTeams.Data.Models;
using FantasyTeams.Data.Storage;
using FantasyTeams.Generator.Models;
using FantasyTeams.Generator.Storage;

namespace FantasyTeams.Generator
{
    public class RandomValueProvider : IRandomValueProvider
    {
        private readonly IRandomNameRepository _nameRepository;
        private readonly ICountryRepository _countryRepository;
        private readonly IConfigProvider _configProvider;
        private readonly Random _random;

        public RandomValueProvider(IRandomNameRepository nameRepository, ICountryRepository countryRepository, 
            IConfigProvider configProvider, Random random)
        {
            _nameRepository = nameRepository;
            _countryRepository = countryRepository;
            _configProvider = configProvider;
            _random = random;
        }

        public async Task<string> GetRandomValue(NameType type)
        {
            var names = (await _nameRepository.GetRandomNames(type)).ToList();
            var index = _random.Next(names.Count);
            return names[index].Value;
        }

        public async Task<Country> GetRandomCountry()
        {
            var countries = (await _countryRepository.GetCountries()).ToList();
            var index = _random.Next(countries.Count);
            return countries[index];
        }

        public async Task<uint> GetRandomAge()
        {
            var ageGaps = await _configProvider.GetAgeGaps();
            return (uint) _random.Next(ageGaps.minAge, ageGaps.maxAge + 1);
        }

        public async Task<int> GetRandomPriceIncreasePercent()
        {
            var priceGaps = await _configProvider.GetPriceIncreaseGaps();
            return _random.Next(priceGaps.minPriceIncrease, priceGaps.maxPriceIncrease + 1);
        }
    }
}