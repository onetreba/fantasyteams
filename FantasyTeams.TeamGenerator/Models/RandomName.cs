﻿using System.ComponentModel.DataAnnotations.Schema;

namespace FantasyTeams.Generator.Models
{
    public enum NameType
    {
        FirstName,
        LastName,
        TeamFirstPart,
        TeamSecondPart
    }

    public class RandomName
    {   
        public int Id { set; get; }

        [Column(TypeName = "VARCHAR(30)")]
        public string Value { set; get; }
        public NameType NameType { set; get; }

        public RandomName()
        {
        }

        public RandomName(string value, NameType type)
        {
            Value = value;
            NameType = type;
        }
    }
}
