﻿using System.ComponentModel.DataAnnotations.Schema;

namespace FantasyTeams.Generator.Models
{
    public class Config
    {
        public int Id { set; get; }

        [Column(TypeName = "VARCHAR(30)")]
        public string Key { set; get; }
        public double Value { set; get; }

        public Config()
        {
        }

        public Config(string key, double value)
        {
            Key = key;
            Value = value;
        }
    }
}