﻿using System.Threading.Tasks;
using FantasyTeams.Data.Models;

namespace FantasyTeams.Generator
{
    public interface ITeamGenerator
    {
        Task<Team> GenerateTeam();
    }
}