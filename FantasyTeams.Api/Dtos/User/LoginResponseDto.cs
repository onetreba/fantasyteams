﻿namespace FantasyTeams.Api.Dtos
{
    public class LoginResponseDto
    {
        public string AccessToken { get; set; }
    }
}