﻿using FantasyTeams.Data.Models;

namespace FantasyTeams.Api.Dtos
{
    public class UserReadDto
    {
        public int Id { get; set; }

        public string Email { get; set; }

        public Role Role { get; set; }
    }
}