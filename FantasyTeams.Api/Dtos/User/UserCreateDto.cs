﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FantasyTeams.Api.Dtos
{
    public class UserCreateDto
    {
        [EmailAddress]
        public string Email { get; set; }

        [StringLength(99, MinimumLength = 3, ErrorMessage = "Password should be at least 3 letters long")]
        public string Password { get; set; }
    }
}
