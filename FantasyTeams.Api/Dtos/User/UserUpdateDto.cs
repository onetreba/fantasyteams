﻿using System.ComponentModel.DataAnnotations;
using FantasyTeams.Data.Models;

namespace FantasyTeams.Api.Dtos
{
    public class UserUpdateDto
    {
        [EmailAddress]
        public string Email { get; set; }

        [StringLength(99, MinimumLength = 3, ErrorMessage = "Password should be at least 3 letters long")]
        public string Password { get; set; }

        public Role Role { get; set; }
    }
}