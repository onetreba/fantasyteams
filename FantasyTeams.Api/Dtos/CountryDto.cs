﻿using System.ComponentModel.DataAnnotations;

namespace FantasyTeams.Api.Dtos
{
    public class CountryDto
    {
        public int Id { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string Name { get; set; }
    }
}