﻿using System.ComponentModel.DataAnnotations;

namespace FantasyTeams.Api.Dtos
{
    public class TransferCreateDto
    {
        public int PlayerId { get; set; }

        [Required]
        [Range(1, double.MaxValue)]
        public double Price { get; set; }
    }
}