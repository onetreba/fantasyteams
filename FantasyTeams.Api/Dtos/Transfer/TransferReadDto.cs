﻿namespace FantasyTeams.Api.Dtos
{
    public class TransferReadDto
    {
        public int Id { get; set; }
        public int PlayerId { get; set; }
        public string Price { get; set; }
    }
}