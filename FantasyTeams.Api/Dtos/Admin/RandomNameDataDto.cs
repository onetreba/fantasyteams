﻿using System.ComponentModel.DataAnnotations;

namespace FantasyTeams.Api.Dtos
{
    public class RandomNameDataDto
    {
        public int Id { set; get; }

        [Required(AllowEmptyStrings = false)]
        public string Value { set; get; }
    }
}