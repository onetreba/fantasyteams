﻿using System.ComponentModel.DataAnnotations;
using FantasyTeams.Generator.Models;

namespace FantasyTeams.Api.Dtos
{
    public class RandomNameDto
    {
        public int Id { set; get; }

        [Required(AllowEmptyStrings = false)]
        public string Value { set; get; }
        public NameType NameType { set; get; }
    }
}