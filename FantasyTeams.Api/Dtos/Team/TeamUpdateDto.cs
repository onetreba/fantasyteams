﻿using System.ComponentModel.DataAnnotations;

namespace FantasyTeams.Api.Dtos
{
    public class TeamUpdateDto
    {
        [Required(AllowEmptyStrings = false)]
        public string Name { set; get; }
        public int CountryId { set; get; }
    }
}