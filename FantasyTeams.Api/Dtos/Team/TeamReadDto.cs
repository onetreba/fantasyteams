﻿namespace FantasyTeams.Api.Dtos
{
    public class TeamReadDto
    {
        public string Name { set; get; }
        public double Balance { set; get; }
        public int CountryId { set; get; }
        public int Id { set; get; }
        public int UserId { set; get; }
    }
}