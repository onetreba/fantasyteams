﻿using FantasyTeams.Data.Models;

namespace FantasyTeams.Api.Dtos
{
    public class PlayerReadDto
    {
        public int Id { set; get; }
        public string FirstName { set; get; }
        public string LastName { set; get; }
        public uint Age { set; get; }
        public decimal MarketValue { set; get; }
        public Position Position { set; get; }

        public int CountryId { set; get; }
        public int TeamId { set; get; }
    }
}