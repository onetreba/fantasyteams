﻿using System.ComponentModel.DataAnnotations;

namespace FantasyTeams.Api.Dtos
{
    public class PlayerUpdateDto
    {
        [Required(AllowEmptyStrings = false)]
        public string FirstName { set; get; }
        [Required(AllowEmptyStrings = false)]
        public string LastName { set; get; }
    }
}