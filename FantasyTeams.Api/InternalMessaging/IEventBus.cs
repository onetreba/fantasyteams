﻿using System;

namespace FantasyTeams.Api.InternalMessaging
{
    public interface IEventBus
    {
        void Publish<TMessage>(TMessage msg);
        IDisposable Subscribe<TMessage>(Action<TMessage> action);
    }
}