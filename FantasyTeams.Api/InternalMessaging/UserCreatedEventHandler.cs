﻿using System;
using System.Threading.Tasks;
using FantasyTeams.Data.Storage;
using FantasyTeams.Generator;
using FantasyTeams.Generator.Storage;
using Microsoft.Extensions.Logging;

namespace FantasyTeams.Api.InternalMessaging
{
    public class UserCreatedEvent
    {
        public int UserId { set; get; }
    }

    public class UserCreatedEventHandler
    {
        private readonly ITeamRepository _teamRepository;
        private readonly ITeamGenerator _teamGenerator;
        private readonly ILogger<UserCreatedEventHandler> _logger;
        private readonly IConfigProvider _configProvider;

        public UserCreatedEventHandler(ITeamRepository repository, ITeamGenerator teamGenerator,
            ILogger<UserCreatedEventHandler> logger, IConfigProvider configProvider)
        {
            _teamRepository = repository;
            _teamGenerator = teamGenerator;
            _logger = logger;
            _configProvider = configProvider;
        }

        public async Task GenerateTeam(UserCreatedEvent e)
        {
            if (!await _configProvider.GetTeamAutoGenerationEnabled())
            {
                _logger.LogInformation($"Team autogeneration disabled");
                return;
            }

            try
            {
                var team = await _teamGenerator.GenerateTeam();
                await _teamRepository.CreateTeam(team, e.UserId);
                await _teamRepository.SaveChanges();

                //you may notify client via messaging here
                _logger.LogInformation($"Team {team.Id} generated");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error on team generation");
            }
        }
    }
}