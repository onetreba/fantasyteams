﻿using System;
using System.Reactive.Linq;
using System.Reactive.Subjects;

namespace FantasyTeams.Api.InternalMessaging
{
    public class ReactiveEventBus : IEventBus
    {
        private readonly ISubject<object> _bus = new Subject<object>();

        public void Publish<TMessage>(TMessage msg)
        {
            _bus.OnNext(msg);
        }


        public IDisposable Subscribe<TMessage>(Action<TMessage> action)
        {
            return _bus
                .OfType<TMessage>()
                .Subscribe((obj) =>
                {
                    var val = (TMessage) obj;
                    action(val);
                });
        }
    }
}