﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace FantasyTeams.Api.InternalMessaging
{
    public class MessageListener : BackgroundService
    {
        private readonly IEventBus _eventBus;
        private readonly IServiceScopeFactory _scopeFactory;

        public MessageListener(IEventBus eventBus, IServiceScopeFactory scopeFactory)
        {
            _eventBus = eventBus;
            _scopeFactory = scopeFactory;
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _eventBus.Subscribe<UserCreatedEvent>(async e => await HandleTeamCreated(e));
            return Task.CompletedTask;
        }

        private async Task HandleTeamCreated(UserCreatedEvent e)
        {
            using (var scope = _scopeFactory.CreateScope())
            {
                var handler = scope.ServiceProvider.GetRequiredService<UserCreatedEventHandler>();
                await handler.GenerateTeam(e);
            }
        }
    }
}
