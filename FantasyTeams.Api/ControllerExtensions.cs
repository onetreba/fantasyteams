using FantasyTeams.Data.Models;
using Microsoft.AspNetCore.Mvc;

namespace FantasyTeams.Api
{
    public static class ControllerExtensions
    {
        public static bool IsCurrentUser(this Controller controller, int? userId)
        {
            return controller.User.FindFirst(Constants.IdClaim)?.Value == userId?.ToString();
        }

        public static bool IsAdmin(this Controller controller)
        {
            return controller.User.IsInRole(Role.Admin.ToString());
        }

        public static bool IsCurrentUserOrAdmin(this Controller controller, int? userId)
        {
            return controller.IsCurrentUser(userId) || controller.IsAdmin();
        }
    }
}