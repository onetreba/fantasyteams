﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FantasyTeams.Api
{
    public static class Constants
    {
        public static string IdClaim = "Id";

        public const string TokenIdentification = "Token";

        public const string UserRole = "User";
        public const string AdminRole = "Admin";
    }
}
