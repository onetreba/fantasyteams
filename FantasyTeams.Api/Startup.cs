using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using FantasyTeams.Api.InternalMessaging;
using FantasyTeams.Data.Storage;
using FantasyTeams.Generator;
using FantasyTeams.Generator.Storage;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace FantasyTeams.Api
{
    public class Startup
    {
        private readonly IWebHostEnvironment _environment;

        public Startup(IConfiguration configuration,  IWebHostEnvironment environment)
        {
            _environment = environment;
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddJsonOptions(opts =>
            {
                var enumConverter = new JsonStringEnumConverter();
                opts.JsonSerializerOptions.Converters.Add(enumConverter);
            });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "FantasyTeams.Api", Version = "v1" });
                c.SwaggerDoc("admin", new OpenApiInfo { Title = "FantasyTeams.Api.Admin", Version = "admin" });
            });

            if (_environment.IsDevelopment())
            {
                services.AddDbContext<ApplicationDbContext>(opt =>
                    opt.UseInMemoryDatabase("InMemoryDb"));

                services.AddDbContext<GeneratorDbContext>(opt =>
                    opt.UseInMemoryDatabase("InMemoryGeneratorDb"));
            }
            else
            {
                services.AddDbContext<ApplicationDbContext>(opt =>
                    opt.UseSqlite(Configuration.GetConnectionString("ApplicationConnection")));

                services.AddDbContext<GeneratorDbContext>(opt =>
                    opt.UseSqlite(Configuration.GetConnectionString("GeneratorConnection")));
            }

            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.RequireHttpsMetadata = false;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidIssuer = AuthConfigs.Issuer,

                        ValidateAudience = true,
                        ValidAudience = AuthConfigs.Audience,
                        ValidateLifetime = true,

                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Configuration[AuthConfigs.TokenKey])),
                        ValidateIssuerSigningKey = true,
                    };
                });

            services.AddScoped<ITeamGenerator, TeamGenerator>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<ITeamRepository, TeamRepository>();
            services.AddScoped<IPlayerRepository, PlayerRepository>();
            services.AddScoped<ITransferRepository, TransferRepository>();
            
            services.AddScoped<CountryRepository>();
            services.AddScoped<ICountryRepository>(provider => 
                new CachingCountryRepository(provider.GetRequiredService<CountryRepository>(), provider.GetRequiredService<IMemoryCache>()));

            services.AddMemoryCache();
            services.AddScoped<ConfigProvider>();
            services.AddScoped<IConfigProvider>(provider => 
                new CachingConfigProviderDecorator(provider.GetRequiredService<ConfigProvider>(), provider.GetRequiredService<IMemoryCache>()));

            services.AddScoped<RandomNameRepository>();
            services.AddScoped<IRandomNameRepository>(provider => 
                new CachingRandomNameRepository(provider.GetRequiredService<RandomNameRepository>(), provider.GetRequiredService<IMemoryCache>()));

            services.AddSingleton<Random>();
            services.AddScoped<IRandomValueProvider, RandomValueProvider>();
            services.AddScoped<RandomNameRepository>();

            services.AddSingleton<IEventBus, ReactiveEventBus>();
            services.AddScoped<UserCreatedEventHandler>();
            services.AddHostedService<MessageListener>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public async void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILogger<Startup> logger)
        {
            logger.LogInformation($"Environment {env.EnvironmentName}");

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "v1");
                    c.SwaggerEndpoint("/swagger/admin/swagger.json", "admin");
                });
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            
            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                var provider = serviceScope.ServiceProvider;
                //only for test and demo purposes
                ////
                var appContext = provider.GetService<ApplicationDbContext>();
                if (appContext.Database.EnsureCreated())
                {
                    logger.LogTrace("Seeding app demo data");
                    ApplicationDbContext.TestSeed(appContext);
                }

                var generatorContext = provider.GetService<GeneratorDbContext>();
                if (generatorContext.Database.EnsureCreated())
                {
                    logger.LogTrace("Seeding generator demo data");
                    GeneratorDbContext.TestSeed(generatorContext);
                }
                ////
                
                //precache often used data
                try
                {
                    await new PreCachingManager(provider.GetService<IRandomNameRepository>(),
                        provider.GetService<ICountryRepository>()).PreCacheData();
                }
                catch (Exception e)
                {
                    logger.LogError(e, "Error on precaching data");
                }
            }
        }
    }
}
