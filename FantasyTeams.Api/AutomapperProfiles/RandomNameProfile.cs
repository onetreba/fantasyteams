﻿using AutoMapper;
using FantasyTeams.Api.Dtos;
using FantasyTeams.Generator.Models;

namespace FantasyTeams.Api.AutomapperProfiles
{
    public class RandomNameProfile : Profile
    {
        public RandomNameProfile()
        {
            CreateMap<RandomName, RandomNameDto>();
            CreateMap<RandomName, RandomNameDataDto>();
        }
    }
}