﻿using AutoMapper;
using FantasyTeams.Api.Dtos;
using FantasyTeams.Data.Models;

namespace FantasyTeams.Api.AutomapperProfiles
{
    public class CountyProfile : Profile
    {
        public CountyProfile()
        {
            CreateMap<Country, CountryDto>().ReverseMap();
        }
    }
}