﻿using AutoMapper;
using FantasyTeams.Api.Dtos;
using FantasyTeams.Data.Models;

namespace FantasyTeams.Api.AutomapperProfiles
{
    public class TransferProfile : Profile
    {
        public TransferProfile()
        {
            CreateMap<TransferCreateDto, Transfer>();
            CreateMap<Transfer, TransferReadDto>();
        }
    }
}