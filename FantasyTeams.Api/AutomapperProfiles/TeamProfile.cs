﻿using AutoMapper;
using FantasyTeams.Api.Dtos;
using FantasyTeams.Data.Models;

namespace FantasyTeams.Api.AutomapperProfiles
{
    public class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<Team, TeamReadDto>();
            CreateMap<TeamUpdateDto, Team>();
        }
    }
}