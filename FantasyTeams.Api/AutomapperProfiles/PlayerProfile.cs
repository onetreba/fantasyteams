﻿using AutoMapper;
using FantasyTeams.Api.Dtos;
using FantasyTeams.Data.Models;

namespace FantasyTeams.Api.AutomapperProfiles
{
    public class PlayerProfile : Profile
    {
        public PlayerProfile()
        {
            CreateMap<Player, PlayerReadDto>();
            CreateMap<PlayerUpdateDto, Player>();
        }
    }
}