﻿namespace FantasyTeams.Api
{
    public class AuthConfigs
    {
        public const string Issuer = "FantasyTeamsAuthority";
        public const string Audience = "FantasyTeamsClient";
        public const int LifetimeMin = 60;

        public const string TokenKey = "Secret:FantasyTeamsTokenKey";
    }
}
