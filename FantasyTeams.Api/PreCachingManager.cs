using System.Threading.Tasks;
using FantasyTeams.Data.Storage;
using FantasyTeams.Generator.Storage;

namespace FantasyTeams.Api
{
    public class PreCachingManager
    {
        private readonly IRandomNameRepository _randomNameRepository;
        private readonly ICountryRepository _countryRepository;

        public PreCachingManager(IRandomNameRepository randomNameRepository, ICountryRepository countryRepository)
        {
            _randomNameRepository = randomNameRepository;
            _countryRepository = countryRepository;
        }

        public async Task PreCacheData()
        {
            await Task.WhenAll(
                _randomNameRepository.GetRandomNames(),
                _countryRepository.GetCountries());
        }
    }
}