﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using FantasyTeams.Api.Dtos;
using FantasyTeams.Api.InternalMessaging;
using FantasyTeams.Data.Models;
using FantasyTeams.Data.Storage;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace FantasyTeams.Api.Controllers
{
    [ApiController]
    [ApiExplorerSettings(GroupName = "v1")]
    [Route("[controller]")]
    public class UsersController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;
        private readonly IEventBus _eventBus;

        public UsersController(IConfiguration configuration, IUserRepository userRepository, IMapper mapper, IEventBus eventBus)
        {
            _configuration = configuration;
            _userRepository = userRepository;
            _mapper = mapper;
            _eventBus = eventBus;
        }

        [HttpPost("/register")]
        public async Task<ActionResult<UserCreateDto>> RegisterUser([FromBody] UserCreateDto user)
        {
            if (await _userRepository.ContainsUser(user.Email))
                return BadRequest($"User {user.Email} already exist");

            var model = _mapper.Map<User>(user);
            await _userRepository.CreateUser(model);
            await _userRepository.SaveChanges();

            //will start async team generation flow if enabled
            _eventBus.Publish(new UserCreatedEvent { UserId = model.Id });

            var readDto = _mapper.Map<UserReadDto>(model);
            return CreatedAtRoute(nameof(GetUser), new { userId = model.Id }, readDto);
        }

        [HttpPost("/login")]
        public async Task<IActionResult> Login([FromForm]string username, [FromForm]string password)
        {
            var identity = await GetIdentityAsync(username, password);
            if (identity == null)
            {
                return BadRequest(new { errorText = "Invalid username or password." });
            }

            var now = DateTime.UtcNow;
            var key = _configuration[AuthConfigs.TokenKey];

            var jwt = new JwtSecurityToken(
                AuthConfigs.Issuer,
                AuthConfigs.Audience,
                notBefore: now,
                claims: identity.Claims,
                expires: now.Add(TimeSpan.FromMinutes(AuthConfigs.LifetimeMin)),
                signingCredentials: new SigningCredentials(new SymmetricSecurityKey(Encoding.ASCII.GetBytes(key)), SecurityAlgorithms.HmacSha256));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            var response = new LoginResponseDto
            {
                AccessToken = encodedJwt
            };

            return Json(response);
        }

        [Authorize]
        [HttpGet("me")]
        public Task<ActionResult<UserReadDto>> GetMe()
        {
            int userId = Int32.Parse(User.FindFirst(Constants.IdClaim).Value);
            return GetUser(userId);
        }

        [Authorize(Roles = Constants.AdminRole)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserReadDto>>> GetUsers()
        {
            var result = await _userRepository.GetUsers();
            return Json(_mapper.Map<IEnumerable<UserReadDto>>(result));
        }

        [Authorize(Roles = Constants.AdminRole)]
        [HttpGet("byEmail")]
        public async Task<ActionResult<UserReadDto>> GetUser([FromBody] string email)
        {
            var result = await _userRepository.GetByEmail(email);
            if (result == null)
                return NotFound("User not found");

            return Json(_mapper.Map<UserReadDto>(result));
        }

        [Authorize(Roles = Constants.AdminRole)]
        [HttpGet("{userId}", Name = nameof(GetUser))]
        public async Task<ActionResult<UserReadDto>> GetUser(int userId)
        {
            var result = await _userRepository.GetById(userId);
            if (result == null)
                return NotFound("User not found");

            return Json(_mapper.Map<UserReadDto>(result));
        }

        [Authorize(Roles = Constants.AdminRole)]
        [HttpPut("{userId}")]
        public async Task<ActionResult<UserReadDto>> UpdateUser(int userId, [FromBody] UserCreateDto user)
        {
            var result = await _userRepository.GetById(userId);
            if (result == null)
                return NotFound("User not found");

            _mapper.Map(user, result);
            result.SetPassword(user.Password);
            await _userRepository.SaveChanges();

            return Ok();
        }

        [Authorize(Roles = Constants.AdminRole)]
        [HttpDelete("{userId}")]
        public async Task<IActionResult> DeleteUser(int userId)
        {
            var user = await _userRepository.GetById(userId);
            if (user == null)
                return NotFound("User not found");

            _userRepository.RemoveUser(user);
            await _userRepository.SaveChanges();
            
            return Ok();
        }

        private async Task<ClaimsIdentity> GetIdentityAsync(string username, string password)
        {
            var person = await _userRepository.GetByEmail(username);
            if (person != null && person.ValidatePassword(password))
            {
                var claims = new List<Claim>
                {
                    new Claim(Constants.IdClaim, person.Id.ToString()),
                    new Claim(ClaimsIdentity.DefaultNameClaimType, person.Email),
                    new Claim(ClaimsIdentity.DefaultRoleClaimType, person.Role.ToString())
                };
                ClaimsIdentity claimsIdentity =
                    new ClaimsIdentity(claims, Constants.TokenIdentification, ClaimsIdentity.DefaultNameClaimType,
                        ClaimsIdentity.DefaultRoleClaimType);
                return claimsIdentity;
            }

            return null;
        }
    }
}