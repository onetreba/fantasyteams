﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FantasyTeams.Api.Dtos;
using FantasyTeams.Data.Models;
using FantasyTeams.Data.Storage;
using FantasyTeams.Generator;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;

namespace FantasyTeams.Api.Controllers
{
    [Authorize]
    [ApiController]
    [ApiExplorerSettings(GroupName = "v1")]
    [Route("[controller]")]
    public class TransfersController : Controller
    {
        private readonly ITransferRepository _transferRepository;
        private readonly IPlayerRepository _playerRepository;
        private readonly ITeamRepository _teamRepository;
        private readonly IRandomValueProvider _randomValueProvider;
        private readonly IMapper _mapper;
        private readonly ILogger<TransfersController> _logger;
        private readonly ApplicationDbContext _context;

        public TransfersController(ITransferRepository transferRepository, IPlayerRepository playerRepository,
            ITeamRepository teamRepository, IRandomValueProvider randomValueProvider, IMapper mapper, 
            ILogger<TransfersController> logger, ApplicationDbContext context)
        {
            _transferRepository = transferRepository;
            _playerRepository = playerRepository;
            _teamRepository = teamRepository;
            _randomValueProvider = randomValueProvider;
            _mapper = mapper;
            _logger = logger;
            _context = context;
        }

        [HttpPost("transfer")]
        public async Task<ActionResult> AddToTransferList([FromBody] TransferCreateDto transfer)
        {
            var player = await _playerRepository.GetPlayerWithTeam(transfer.PlayerId);
            var userId = Int32.Parse(User.FindFirst(Constants.IdClaim).Value);

            if (player?.Team.UserId == userId)
            {
                if (await _transferRepository.Contains(player.Id))
                    return BadRequest("Player is already in transfer list");

                var model = _mapper.Map<TransferCreateDto, Transfer>(transfer);
                model.UserId = userId;
                await _transferRepository.AddTransfer(model);
                await _transferRepository.SaveChanges();

                return CreatedAtAction(nameof(GetTransfer), new {transferId = model.Id}, model);
            }

            return Forbid("You are not allowed to edit this player");
        }

        [HttpDelete("transfer")]
        public async Task<ActionResult> RemoveFromTransferList([FromBody] int transferId)
        {
            var transfer = await _transferRepository.GetTransfer(transferId);
            string userId = User.FindFirst(Constants.IdClaim).Value;

            if (transfer?.UserId.ToString() == userId)
            {
                if (transfer == null)
                    return NotFound("Transfer not found");

                _transferRepository.RemoveTransfer(transfer);
                await _transferRepository.SaveChanges();

                return Ok();
            }

            return Forbid("You are not allowed to delete this transfer");
        }

        [HttpPost("buy")]
        public async Task<ActionResult> Buy([FromBody] int transferId)
        {
            var transfer = await _transferRepository.GetTransfer(transferId);
            if (transfer == null)
                return NotFound("Transfer not found");

            var userId = Int32.Parse(User.FindFirst(Constants.IdClaim).Value);
            if (transfer.UserId == userId)
                return BadRequest("You cannot buy your player");

            var team = await _teamRepository.GetUserTeam(userId);
            if (team == null)
                return BadRequest("You have no team");

            var currentTeam = await _teamRepository.GetTeam(transfer.UserId);

            if (team.Balance - transfer.Price < 0)
                return BadRequest("Not enough money");

            try
            {
                var priceIncreasePercent = await _randomValueProvider.GetRandomPriceIncreasePercent();
                using (var transaction = await _context.Database.BeginTransactionAsync())
                {
                    var player = await _playerRepository.GetPlayer(transfer.PlayerId);
                    var priceDelta = player.MarketValue * priceIncreasePercent / 100;

                    player.TeamId = team.Id;
                    player.MarketValue += priceDelta;

                    team.Balance -= transfer.Price;
                    currentTeam.Balance += transfer.Price;

                    await _playerRepository.SaveChanges();
                    await _teamRepository.SaveChanges();

                    transaction.Commit();
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error on trying to buy player");
            }

            return Ok();
        }

        [HttpGet("{transferId}")]
        public async Task<ActionResult<TransferReadDto>> GetTransfer(int transferId)
        {
            var transfer = await _transferRepository.GetTransfer(transferId);

            if (transfer == null)
                return NotFound("Transfer not found");

            return Json(_mapper.Map<TransferReadDto>(transfer));
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<TransferReadDto>>> GetTransfers()
        {
            var transfer = await _transferRepository.GetTransferList();

            return Json(_mapper.Map<IEnumerable<TransferReadDto>>(transfer));
        }

        [HttpGet("user/{userId}")]
        public async Task<ActionResult<IEnumerable<TransferReadDto>>> GetUserTransfers(int userId)
        {
            var transfer = await _transferRepository.GetTransferList(userId);

            return Json(_mapper.Map<IEnumerable<TransferReadDto>>(transfer));
        }
    }
}
