﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FantasyTeams.Api.Dtos;
using FantasyTeams.Data.Storage;
using FantasyTeams.Generator.Models;
using FantasyTeams.Generator.Storage;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FantasyTeams.Api.Controllers
{
    [Authorize(Roles = Constants.AdminRole)]
    [ApiController]
    [ApiExplorerSettings(GroupName = "admin")]
    [Route("admin/random-data")]
    public class AdminRandomNameController : Controller
    {
        private readonly RandomNameRepository _randomNameRepository;
        private readonly IMapper _mapper;

        public AdminRandomNameController(RandomNameRepository randomNameRepository, IMapper mapper)
        {
            _randomNameRepository = randomNameRepository;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<RandomName>>> GetRandomNames()
        {
            var names = await _randomNameRepository.GetRandomNames();
            return Json(_mapper.Map<IEnumerable<RandomNameDto>>(names));
        }

        [HttpGet("{nameType}")]
        public async Task<ActionResult<IEnumerable<RandomName>>> GetRandomNamesByType(NameType nameType)
        {
            var names = await _randomNameRepository.GetRandomNames(nameType);
            return Json(_mapper.Map<IEnumerable<RandomNameDataDto>>(names));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<RandomName>> GetRandomName(int id)
        {
            var name = await _randomNameRepository.GetRandomName(id);
            if (name == null)
                return NotFound();

            return Json(_mapper.Map<IEnumerable<RandomNameDto>>(name));
        }

        [HttpPost("{nameType}")]
        public async Task<ActionResult<IEnumerable<RandomName>>> AddRandomName(NameType nameType, string value)
        {
            var name = new RandomName(value, nameType);
            await _randomNameRepository.AddName(name);
            await _randomNameRepository.SaveChanges();
            var dto = _mapper.Map<IEnumerable<RandomNameDto>>(name);
            return CreatedAtAction(nameof(GetRandomName), new { name.Id }, dto);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<IEnumerable<RandomName>>> DeleteRandomName(int id)
        {
            var name = await _randomNameRepository.GetRandomName(id);
            if (name == null)
                return NotFound();

            _randomNameRepository.DeleteRandomName(name);
            await _randomNameRepository.SaveChanges();

            return Ok();
        }
    }
}
