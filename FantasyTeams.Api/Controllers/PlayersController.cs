﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FantasyTeams.Api.Dtos;
using FantasyTeams.Data.Models;
using FantasyTeams.Data.Storage;
using Microsoft.AspNetCore.Authorization;

namespace FantasyTeams.Api.Controllers
{
    [Authorize]
    [ApiController]
    [ApiExplorerSettings(GroupName = "v1")]
    [Route("[controller]")]
    public class PlayersController : Controller
    {
        private readonly IPlayerRepository _playerRepository;
        private readonly ITeamRepository _teamRepository;
        private readonly IMapper _mapper;

        public PlayersController(IPlayerRepository playerRepository, ITeamRepository teamRepository, IMapper mapper)
        {
            _playerRepository = playerRepository;
            _teamRepository = teamRepository;
            _mapper = mapper;
        }

        [Authorize(Roles = Constants.AdminRole)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PlayerReadDto>>> GetPlayers()
        {
            var result = await _playerRepository.GetPlayers();
            if (result == null)
                return NotFound("Player not found");

            return Json(_mapper.Map<IEnumerable<PlayerReadDto>>(result));
        }

        [HttpGet("team/{teamId}")]
        public async Task<ActionResult<IEnumerable<PlayerReadDto>>> GetPlayers(int teamId)
        {
            var team = await _teamRepository.GetTeam(teamId);

            if (team == null)
                return NotFound("Team not found");

            if (this.IsCurrentUserOrAdmin(team.Id))
            {
                var result = await _playerRepository.GetPlayers(teamId);
                
                return Json(_mapper.Map<IEnumerable<PlayerReadDto>>(result));
            }

            return Forbid("You are not allowed to view this team");
        }

        [HttpGet("{playerId}")]
        public async Task<ActionResult<PlayerReadDto>> GetPlayer(int playerId)
        {
            var player = await _playerRepository.GetPlayerWithTeam(playerId);

            if (this.IsCurrentUserOrAdmin(player?.Team.UserId))
            {
                if (player == null)
                    return NotFound("Player not found");

                return Json(_mapper.Map<PlayerReadDto>(player));
            }

            return Forbid("You are not allowed to view this player");
        }

        [HttpPut("{playerId}")]
        public async Task<ActionResult<TeamReadDto>> UpdatePlayer(int playerId, [FromBody] PlayerUpdateDto model)
        {
            var player = await _playerRepository.GetPlayerWithTeam(playerId);

            if (this.IsCurrentUserOrAdmin(player?.Team.UserId))
            {
                if (player == null)
                    return NotFound("Player not found");

                _mapper.Map(model, player);
                await _playerRepository.SaveChanges();
                return Ok();
            }

            return Forbid("You are not allowed to edit this team");
        }
    }
}
