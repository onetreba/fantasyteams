﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FantasyTeams.Api.Dtos;
using FantasyTeams.Data.Models;
using FantasyTeams.Data.Storage;
using FantasyTeams.Generator.Storage;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FantasyTeams.Api.Controllers
{
    [Authorize(Roles = Constants.AdminRole)]
    [ApiController]
    [ApiExplorerSettings(GroupName = "admin")]
    [Route("admin/country")]
    public class AdminCountryController : Controller
    {
        private readonly ICountryRepository _countryRepository;
        private readonly IMapper _mapper;

        public AdminCountryController(ICountryRepository countryRepository, IMapper mapper)
        {
            _countryRepository = countryRepository;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<CountryDto>>> GetCountries()
        {
            var countries = await _countryRepository.GetCountries();
            return Json(_mapper.Map<IEnumerable<CountryDto>>(countries));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<CountryDto>> GetCountry(int id)
        {
            var country = await _countryRepository.GetCountry(id);
            if (country == null)
                return NotFound($"No country with id {id}");

            return Json(_mapper.Map<CountryDto>(country));
        }

        [HttpPost]
        public async Task<ActionResult<CountryDto>> AddCountry(string countryName)
        {
            var country = new Country(countryName);
            await _countryRepository.AddCountry(country);
            await _countryRepository.SaveChanges();

            return CreatedAtAction(nameof(GetCountry), new {country.Id}, _mapper.Map<CountryDto>(country));
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> UpdateCountry(int id, string countryName)
        {
            var country = await _countryRepository.GetCountry(id);
            if (country == null)
                return NotFound($"No country with id {id}");

            country.Name = countryName;
            await _countryRepository.SaveChanges();

            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteCountry(int id)
        {
            var country = await _countryRepository.GetCountry(id);
            if (country == null)
                return NotFound($"No country with id {id}");

            _countryRepository.DeleteCountry(country);
            await _countryRepository.SaveChanges();

            return Ok();
        }
    }
}
