﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FantasyTeams.Generator.Storage;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FantasyTeams.Api.Controllers
{
    [Authorize(Roles = Constants.AdminRole)]
    [ApiController]
    [ApiExplorerSettings(GroupName = "admin")]
    [Route("admin/config")]
    public class AdminConfigController : Controller
    {
        private readonly IConfigProvider _configProvider;

        public AdminConfigController(IConfigProvider configProvider)
        {
            _configProvider = configProvider;
        }

        [HttpGet("age-gap")]
        public async Task<ActionResult<(int minAge, int maxAge)>> GetAgeGaps()
        {
            var gaps = await _configProvider.GetAgeGaps();
            return Json(new {gaps.minAge, gaps.maxAge});
        }

        [HttpPost("age-gap")]
        public async Task<ActionResult> SetAgeGaps(int minAge, int maxAge)
        {
            await _configProvider.SetAgeGaps((minAge, maxAge));
            return Ok();
        }

        [HttpGet("player-start-value")]
        public async Task<ActionResult<int>> GetPlayerStartValue()
        {
            var value = await _configProvider.GetPlayerStartValue();
            return Json(value);
        }

        [HttpPost("player-start-value")]
        public async Task<ActionResult> SetPlayerStartValue(int value)
        {
            await _configProvider.SetPlayerStartValue(value);
            return Ok();
        }

        [HttpGet("team-start-balance")]
        public async Task<ActionResult<int>> GetTeamStartBalance()
        {
            var value = await _configProvider.GetTeamStartBalance();
            return Json(value);
        }

        [HttpPost("team-start-balance")]
        public async Task<ActionResult> SetTeamStartBalance(int value)
        {
            await _configProvider.SetTeamStartBalance(value);
            return Ok();
        }

        [HttpGet("goalkeepers-count")]
        public async Task<ActionResult<(int minAge, int maxAge)>> GetGoalkeepersCount()
        {
            var value = await _configProvider.GetGoalkeepersCount();
            return Json(value);
        }

        [HttpPost("goalkeepers-count")]
        public async Task<ActionResult> SetGoalkeepersCount(int value)
        {
            await _configProvider.SetGoalkeepersCount(value);
            return Ok();
        }

        [HttpGet("defenders-count")]
        public async Task<ActionResult<(int minAge, int maxAge)>> GetDefendersCount()
        {
            var value = await _configProvider.GetDefendersCount();
            return Json(value);
        }

        [HttpPost("defenders-count")]
        public async Task<ActionResult> SetDefendersCount(int value)
        {
            await _configProvider.SetDefendersCount(value);
            return Ok();
        }

        [HttpGet("midfielders-count")]
        public async Task<ActionResult<(int minAge, int maxAge)>> GetMidfieldersCount()
        {
            var value = await _configProvider.GetMidfieldersCount();
            return Json(value);
        }

        [HttpPost("midfielders-count")]
        public async Task<ActionResult> SetMidfieldersCount(int value)
        {
            await _configProvider.SetMidfieldersCount(value);
            return Ok();
        }

        [HttpGet("attackers-count")]
        public async Task<ActionResult<(int minAge, int maxAge)>> GetAttackersCount()
        {
            var value = await _configProvider.GetAttackersCount();
            return Json(value);
        }

        [HttpPost("attackers-count")]
        public async Task<ActionResult> SetAttackersCount(int value)
        {
            await _configProvider.SetAttackersCount(value);
            return Ok();
        }
    }
}
