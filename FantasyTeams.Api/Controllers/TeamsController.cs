﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using FantasyTeams.Api.Dtos;
using FantasyTeams.Data.Models;
using FantasyTeams.Data.Storage;
using FantasyTeams.Generator;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using FantasyTeams.Api;

namespace FantasyTeams.Api.Controllers
{
    [Authorize]
    [ApiController]
    [ApiExplorerSettings(GroupName = "v1")]
    [Route("[controller]")]
    public class TeamsController : Controller
    {
        private readonly ITeamRepository _teamRepository;
        private readonly IMapper _mapper;
        private readonly ITeamGenerator _generator;

        public TeamsController(ITeamRepository teamRepository, IMapper mapper, ITeamGenerator generator)
        {
            _teamRepository = teamRepository;
            _mapper = mapper;
            _generator = generator;
        }

        [HttpPost("generate")]
        public async Task<ActionResult<TeamReadDto>> GenerateTeam()
        {
            int userId = Int32.Parse(User.FindFirst(Constants.IdClaim).Value);
            if (await _teamRepository.HasTeam(userId))
                return BadRequest(Json("User already has a team"));

            var team = await _generator.GenerateTeam();
            await _teamRepository.CreateTeam(team, userId);
            await _teamRepository.SaveChanges();

            return CreatedAtAction(nameof(GetTeam), new {teamId = team.Id}, _mapper.Map<TeamReadDto>(team));
        }

        [Authorize(Roles = Constants.AdminRole)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TeamReadDto>>> GetTeams()
        {
            var result = await _teamRepository.GetTeams();

            return Json(_mapper.Map<IEnumerable<TeamReadDto>>(result));
        }

        [Authorize(Roles = Constants.AdminRole)]
        [HttpGet("{teamId}")]
        public async Task<ActionResult<TeamReadDto>> GetTeam(int teamId)
        {
            var result = await _teamRepository.GetTeam(teamId);
            if (result == null)
                return NotFound("Team not found");

            return Json(_mapper.Map<TeamReadDto>(result));
        }

        [HttpGet("my")]
        public async Task<ActionResult<TeamReadDto>> GetMyTeam()
        {
            int userId = Int32.Parse(User.FindFirst(Constants.IdClaim).Value);
            var result = await _teamRepository.GetUserTeam(userId);
            if (result == null)
                return NotFound("Team not found");

            return Json(_mapper.Map<TeamReadDto>(result));
        }

        [HttpPut("{teamId}")]
        public async Task<ActionResult<TeamReadDto>> UpdateTeam(int teamId, [FromBody] TeamUpdateDto model)
        {
            var team = await _teamRepository.GetTeam(teamId);

            if (this.IsCurrentUserOrAdmin(team?.UserId))
            {
                if (team == null)
                    return NotFound("Team not found");

                _mapper.Map(model, team);
                await _teamRepository.SaveChanges();
                return Ok();
            }

            return Forbid("You are not allowed to edit this team");
        }

        [HttpDelete("teamId")]
        public async Task<ActionResult> DeleteTeam(int teamId)
        {
            var team = await _teamRepository.GetTeam(teamId);

            if (this.IsCurrentUserOrAdmin(team?.UserId))
            {
                if (team == null)
                    return NotFound("Team not found");

                _teamRepository.DeleteTeam(team);
                await _teamRepository.SaveChanges();
                return Ok();
            }

            return Forbid("You are not allowed to delete this team");
        }
    }
}