﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FantasyTeams.Data.Models;

namespace FantasyTeams.Data.Storage
{
    public interface ITransferRepository
    {
        Task<IEnumerable<Transfer>> GetTransferList();
        Task<IEnumerable<Transfer>> GetTransferList(int userId);
        Task<Transfer> GetTransfer(int transferId);
        ValueTask AddTransfer(Transfer transfer);
        void RemoveTransfer(Transfer transfer);
        Task<bool> Contains(int playerId);
        Task<bool> SaveChanges();
    }
}