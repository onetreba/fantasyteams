﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FantasyTeams.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace FantasyTeams.Data.Storage
{
    public class TeamRepository : RepositoryBase, ITeamRepository
    {
        public TeamRepository(ApplicationDbContext context, ILogger<TeamRepository> logger)
            : base(context, logger)
        {
        }

        public async ValueTask CreateTeam(Team team, int userId)
        {
            team.UserId = userId;
            await Context.Teams.AddAsync(team);
        }

        public async Task<IEnumerable<Team>> GetTeams()
        {
            return await Context.Teams.ToListAsync();
        }

        public Task<Team> GetTeam(int teamId)
        {
            return Context.Teams.SingleOrDefaultAsync(x => x.Id == teamId);
        }

        public Task<Team> GetUserTeam(int userId)
        {
            return Context.Teams.SingleOrDefaultAsync(x => x.UserId == userId);
        }

        public Task<bool> HasTeam(int userId)
        {
            return Context.Teams.AnyAsync(x => x.UserId == userId);
        }

        public void DeleteTeam(Team team)
        {
            Context.Teams.Remove(team);
        }
    }
}