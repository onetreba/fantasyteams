﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FantasyTeams.Data.Models;
using Microsoft.Extensions.Caching.Memory;

namespace FantasyTeams.Data.Storage
{
    public class CachingCountryRepository : ICountryRepository
    {
        private const string Key = "CountryCache";
        private readonly ICountryRepository _repository;
        private readonly IMemoryCache _memoryCache;

        public CachingCountryRepository(ICountryRepository repository, IMemoryCache memoryCache)
        {
            _repository = repository;
            _memoryCache = memoryCache;
        }

        public async Task<IEnumerable<Country>> GetCountries()
        {
            if (!_memoryCache.TryGetValue(Key, out Dictionary<int, Country> cache))
            {
                cache = (await _repository.GetCountries()).ToDictionary(x => x.Id);
                _memoryCache.Set(Key, cache);
            }

            return cache.Values;
        }

        public async Task<Country> GetCountry(int id)
        {
            return (await GetCountries()).SingleOrDefault(x => x.Id == id);
        }

        public ValueTask AddCountry(Country country)
        {
            if (_memoryCache.TryGetValue(Key, out Dictionary<int, Country> cache))
            {
                cache[country.Id] = country;
                _memoryCache.Set(Key, cache);
            }

            return _repository.AddCountry(country);
        }

        public void DeleteCountry(Country country)
        {
            if (_memoryCache.TryGetValue(Key, out Dictionary<int, Country> cache))
            {
                cache.Remove(country.Id);
                _memoryCache.Set(Key, cache);
            }

            _repository.DeleteCountry(country);
        }

        public Task<bool> SaveChanges()
        {
            return _repository.SaveChanges();
        }
    }
}