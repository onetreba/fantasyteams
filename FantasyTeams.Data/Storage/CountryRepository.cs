﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FantasyTeams.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace FantasyTeams.Data.Storage
{
    public class CountryRepository : RepositoryBase, ICountryRepository
    {
        public CountryRepository(ApplicationDbContext context, ILogger<CountryRepository> logger)
            : base(context, logger)
        {
        }

        public async Task<IEnumerable<Country>> GetCountries()
        {
            return await Context.Countries.ToListAsync();
        }

        public Task<Country> GetCountry(int id)
        {
            return Context.Countries.FirstOrDefaultAsync(x => x.Id == id);
        }

        public async ValueTask AddCountry(Country country)
        {
            await Context.AddAsync(country);
        }

        public void DeleteCountry(Country country)
        {
            Context.Remove(country);
        }
    }
}