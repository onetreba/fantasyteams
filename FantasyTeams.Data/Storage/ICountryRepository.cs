﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FantasyTeams.Data.Models;

namespace FantasyTeams.Data.Storage
{
    public interface ICountryRepository
    {
        Task<IEnumerable<Country>> GetCountries();
        Task<Country> GetCountry(int id);
        ValueTask AddCountry(Country country);
        void DeleteCountry(Country country);
        Task<bool> SaveChanges();
    }
}