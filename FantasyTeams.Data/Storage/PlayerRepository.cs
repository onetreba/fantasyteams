﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FantasyTeams.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace FantasyTeams.Data.Storage
{
    public class PlayerRepository : RepositoryBase, IPlayerRepository
    {
        public PlayerRepository(ApplicationDbContext context, ILogger<PlayerRepository> logger) 
            : base(context, logger)
        {
        }

        public async Task<IEnumerable<Player>> GetPlayers()
        {
            return await Context.Players.ToListAsync();
        }

        public async Task<IEnumerable<Player>> GetPlayers(int teamId)
        {
            return await Context.Players.Where(x => x.TeamId == teamId).ToListAsync();
        }

        public Task<Player> GetPlayerWithTeam(int playerId)
        {
            return Context.Players.Include(x => x.Team).SingleOrDefaultAsync(x => x.Id == playerId);
        }

        public Task<Player> GetPlayer(int playerId)
        {
            return Context.Players.SingleOrDefaultAsync(x => x.Id == playerId);
        }
    }
}