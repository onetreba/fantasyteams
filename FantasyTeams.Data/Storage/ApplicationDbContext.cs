﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using FantasyTeams.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;

namespace FantasyTeams.Data.Storage
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<Player> Players { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Transfer> TransferList { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<Team>()
                .HasMany(x => x.Players)
                .WithOne(x => x.Team!)
                .HasForeignKey(x => x.TeamId);

            modelBuilder
                .Entity<Player>()
                .HasOne(x => x.Team!)
                .WithMany(x => x.Players)
                .HasForeignKey(x => x.TeamId);
        }

        //used only for debug and demo purposes
        [Conditional("DEBUG")]
        public static void TestSeed(ApplicationDbContext context)
        {
            var country = new Country("Germany");
            var user = new User("qwerty@mail.com", "12345", Role.Admin);

            var team = new Team
            {
                Name = "TestTeam",
                Country = country,
                Players = new List<Player>
                {
                    new Player
                    {
                        Country = country,
                        Age = 20,
                        FirstName = "TestFirstName",
                        LastName = "TestLastName",
                        Position = Position.Attacker,
                        MarketValue = 10000000
                    }
                },
                User = user
            };
            context.Teams.Add(team);

            context.Countries.AddRange(
                country,
                new Country("England"),
                new Country("Brazil"),
                new Country("Argentina"),
                new Country("France"),
                new Country("Italy"),
                new Country("Spain")
            );

            var admin = new User("admin@mail.com", "12345", Role.Admin);
            context.Users.Add(admin);

            context.SaveChanges();
        }
    }
}
