﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FantasyTeams.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace FantasyTeams.Data.Storage
{
    public class TransferRepository : RepositoryBase, ITransferRepository
    {
        public TransferRepository(ApplicationDbContext context, ILogger<TransferRepository> logger) 
            : base(context, logger)
        {
        }

        public async Task<IEnumerable<Transfer>> GetTransferList()
        {
            return await Context.TransferList.ToListAsync();
        }

        public async Task<IEnumerable<Transfer>> GetTransferList(int userId)
        {
            return await Context.TransferList.Where(x => x.UserId == userId).ToListAsync();
        }

        public Task<Transfer> GetTransfer(int transferId)
        {
            return Context.TransferList.SingleOrDefaultAsync(x => x.Id == transferId);
        }

        public async ValueTask AddTransfer(Transfer transfer)
        {
            await Context.AddAsync(transfer);
        }

        public void RemoveTransfer(Transfer transfer)
        {
            Context.Remove(transfer);
        }

        public Task<bool> Contains(int playerId)
        {
            return Context.Players.AnyAsync(x => x.Id == playerId);
        }
    }
}