﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace FantasyTeams.Data.Storage
{
    public abstract class RepositoryBase
    {
        protected readonly ApplicationDbContext Context;
        protected readonly ILogger Logger;

        protected RepositoryBase(ApplicationDbContext context, ILogger logger)
        {
            Context = context;
            Logger = logger;
        }

        public async Task<bool> SaveChanges()
        {
            try
            {
                return await Context.SaveChangesAsync() > 0;
            }
            catch (Exception e)
            {
                Logger.LogError(e, "Failed to save changes");
                throw;
            }
        }
    }
}