﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FantasyTeams.Data.Models;

namespace FantasyTeams.Data.Storage
{
    public interface ITeamRepository
    {
        ValueTask CreateTeam(Team team, int userId);
        Task<IEnumerable<Team>> GetTeams();
        Task<Team> GetTeam(int teamId);
        Task<Team> GetUserTeam(int userId);
        Task<bool> HasTeam(int userId);
        void DeleteTeam(Team team);
        Task<bool> SaveChanges();
    }
}