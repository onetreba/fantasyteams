﻿using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using FantasyTeams.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;

namespace FantasyTeams.Data.Storage
{
    public class UserRepository : RepositoryBase, IUserRepository
    {
        public UserRepository(ApplicationDbContext context, ILogger<UserRepository> logger)
            : base(context, logger)
        {
        }

        public async Task<IEnumerable<User>> GetUsers()
        {
            return await Context.Users.ToListAsync();
        }

        public Task<User> GetByEmail(string login)
        {
            return Context.Users.FirstOrDefaultAsync(x => x.Email == login);
        }

        public Task<User> GetById(int id)
        {
            return Context.Users.FirstOrDefaultAsync(x => x.Id == id);
        }

        public async ValueTask CreateUser(User user)
        {
            await Context.AddAsync(user);
        }

        public void RemoveUser(User user)
        {
            Context.Remove(user);
        }

        public Task<bool> ContainsUser(string email)
        {
            return Context.Users.AnyAsync(x => x.Email == email);
        }
    }
}
