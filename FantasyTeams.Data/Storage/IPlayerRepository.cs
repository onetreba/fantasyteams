﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FantasyTeams.Data.Models;

namespace FantasyTeams.Data.Storage
{
    public interface IPlayerRepository
    {
        Task<IEnumerable<Player>> GetPlayers();
        Task<IEnumerable<Player>> GetPlayers(int teamId);
        Task<Player> GetPlayerWithTeam(int playerId);
        Task<Player> GetPlayer(int playerId);
        Task<bool> SaveChanges();
    }
}