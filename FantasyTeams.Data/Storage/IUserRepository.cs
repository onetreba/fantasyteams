﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FantasyTeams.Data.Models;

namespace FantasyTeams.Data.Storage
{
    public interface IUserRepository
    {
        Task<IEnumerable<User>> GetUsers();
        Task<User> GetByEmail(string login);
        Task<User> GetById(int id);
        ValueTask CreateUser(User user);
        void RemoveUser(User user);
        Task<bool> ContainsUser(string email);
        Task<bool> SaveChanges();
    }
}