﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace FantasyTeams.Data.Models
{
    [Index(nameof(Name), IsUnique = true)]
    public class Country
    {
        public int Id { set; get; }

        [Column(TypeName = "VARCHAR(30)")]
        public string Name { set; get; }

        public Country(string name)
        {
            Name = name;
        }

        public Country()
        {
        }
    }
}