﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace FantasyTeams.Data.Models
{
    public class Player
    {
        public int Id { set; get; }

        [Column(TypeName = "VARCHAR(30)")]
        public string FirstName { set; get; }

        [Column(TypeName = "VARCHAR(30)")]
        public string LastName { set; get; }
        public uint Age { set; get; }
        public decimal MarketValue { set; get; }
        public Position Position { set; get; }

        [ForeignKey("Country")]
        public int CountryId { set; get; }
        public Country Country { set; get; }

        public int TeamId { set; get; }
        public Team Team { set; get; }
    }
}
