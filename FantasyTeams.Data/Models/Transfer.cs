﻿using System.ComponentModel.DataAnnotations.Schema;

namespace FantasyTeams.Data.Models
{
    public class Transfer
    {
        public int Id { set; get; }
        public double Price { set; get; }

        [ForeignKey("Player")]
        public int PlayerId { set; get; }
        public Player Player { set; get; }

        [ForeignKey("User")]
        public int UserId { set; get; }
        public User User { set; get; }
    }
}