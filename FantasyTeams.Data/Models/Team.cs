﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FantasyTeams.Data.Models
{
    public class Team
    {
        public int Id { set; get; }
        
        [Column(TypeName = "VARCHAR(30)")]
        public string Name { set; get; }
        public double Balance { set; get; }

        public int CountryId { set; get; }
        public Country Country { set; get; }

        public int UserId { set; get; }
        public User User { set; get; }

        public ICollection<Player> Players { set; get; } = new List<Player>();

        public Team()
        {
        }

        public Team(string name)
        {
            Name = name;
        }

        public override string ToString()
        {
            var playersList = Players.Select(x =>
                $"{x.FirstName} {x.LastName} {x.Position} {x.Country.Name} {x.Age} {x.MarketValue}$");
            var playersStr = string.Join(Environment.NewLine, playersList);

            return
                $"{Name} {Balance}{Environment.NewLine}{playersStr}";
        }
    }
}