﻿namespace FantasyTeams.Data.Models
{
    public enum Role
    {
        User,
        Admin
    }
}