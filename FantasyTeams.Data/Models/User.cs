﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace FantasyTeams.Data.Models
{
    [Index(nameof(Email), IsUnique = true)]
    public class User
    {
        private static readonly IPasswordHasher<User> Hasher = new PasswordHasher<User>();

        public User()
        {
        }

        public User(string email, string password, Role role)
        {
            Email = email;
            Role = role;
            SetPassword(password);
        }

        public int Id { get; set; }

        [Column(TypeName = "VARCHAR(100)")]
        public string Email { get; set; }

        public Role Role { get; set; }

        public string PasswordHash
        {
            get;
            private set;
        }

        public void SetPassword(string password)
        {
            PasswordHash = Hasher.HashPassword(this, password);
        }

        public bool ValidatePassword(string password)
        {
            return Hasher.VerifyHashedPassword(this, PasswordHash, password) == PasswordVerificationResult.Success;
        }
    }
}