﻿namespace FantasyTeams.Data.Models
{
    public enum Position
    {
        Goalkeeper,
        Defender,
        Midfielder,
        Attacker
    }
}